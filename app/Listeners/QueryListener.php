<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class QueryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(env('APP_ENV','production') == 'local'){
            $sql = str_replace("?","'%s'",$event->sql);
            $log = vsprintf($sql,$event->bindings);
            \Log::info($log);
        }
    }
}
